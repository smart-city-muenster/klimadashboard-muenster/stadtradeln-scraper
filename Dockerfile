FROM python:3.11-slim-bookworm

WORKDIR /app

COPY install-chrome.sh ./install-chrome.sh
RUN sh install-chrome.sh

COPY requirements.txt ./requirements.txt
RUN pip install -r requirements.txt

COPY data ./data

COPY index.py ./index.py

ENTRYPOINT ["python", "index.py"]