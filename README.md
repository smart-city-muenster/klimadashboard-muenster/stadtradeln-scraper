# Stadtradeln Scraper

## Description

This project is a scraper for the Stadtradeln website. It allows users to retrieve data about cycling activities in their city.

## Features

- Scrapes data from the Stadtradeln website

## Installation

**Please check the [Devcotainer](#devcontainer) section for a more convenient way to set up the project.**

1. Clone the repository: `git clone https://gitlab.opencode.de/smart-city-muenster/klimadashboard-muenster/stadtradeln-scraper`
2. Install the required dependencies: `pip install -r requirements.txt`

## Usage

1. Run the script by executing the following command:

   ```
   python index.py [output_dir]
   ```

   Replace `[output_dir]` with the optional output directory path where you want to save the CSV file. If no output directory is provided, the CSV file will be saved in the `data` directory.

2. The script will gather data for the specified cities and save it in a CSV file named `stadtradeln_data.csv`. If an output directory is provided, the CSV file will be saved there instead.

3. Make sure you have Chrome installed on your machine, as the script uses Selenium WebDriver with Chrome to scrape data from the Stadtradeln website.

4. You can modify the list of cities in the script by updating the `cities` variable. Each city should be specified as a list containing the city name and its display name.

5. If any errors occur during the data gathering process, they will be printed to the console.

6. Enjoy using the Stadtradeln scraper!

## Docker

You can also run the scraper using Docker. To do this, follow these steps:

1. Build the Docker image by executing the following command:

   ```
   docker build -t stadtradeln-scraper .
   ```

2. Run the Docker container by executing the following command:
   ```
   docker run -v $(pwd)/data:/app/data stadtradeln-scraper
   ```

This command will mount the `data` directory to the `/app/data` directory inside the Docker container. The CSV file will be saved in the `data` directory on your host machine.

## Devcontainer

This project includes a `.devcontainer` directory with a `devcontainer.json` file that allows you to use Visual Studio Code's Remote - Containers extension to develop inside a Docker container. To use this feature, follow these steps:

1. Install the Remote - Containers extension in Visual Studio Code.

2. Open the project in Visual Studio Code.

3. Click on the green icon in the lower-left corner of the window and select "Reopen in Container".

4. Visual Studio Code will build the Docker image and start the container. You can now develop inside the container.
