import sys
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from bs4 import BeautifulSoup
from webdriver_manager.chrome import ChromeDriverManager
import time
import pandas as pd
from datetime import datetime

# read optional output dir path
output_dir = sys.argv[1] if len(sys.argv) > 1 else None

# URL der Webseite
url = "https://www.stadtradeln.de/"

# Zeitstempel für den Start der Datensammlung
timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
# Selenium WebDriver für Chrome initialisieren mit ChromeDriverManager
service = Service(executable_path=ChromeDriverManager().install())
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--no-sandbox")
driver = webdriver.Chrome(options=options)


def getKilometerFromCity(city):
    try:
        request_url = url + city
        # Webseite öffnen
        driver.get(request_url)
        # Warte kurz, damit die Seite vollständig geladen wird
        time.sleep(5)
        # HTML der Seite erhalten
        html = driver.page_source
        # BeautifulSoup Objekt erstellen
        soup = BeautifulSoup(html, "html.parser")
        # Alle Listenelemente mit der Klasse 'icon-road' und dem Text 'gefahrene Kilometer' finden
        element = soup.find_all(
            "p", attrs={"class": "icon-road"}, string="gefahrene Kilometer"
        )
        kilometers_clean = 0
        if len(element) == 0:
            raise ValueError("Kilometer nicht gefunden")
        else:
            # Das Parent Element enthält die Node <h3>{kilometer}</h3> danach wird gesucht und der Text ausgegeben
            kilometers = element[0].parent.contents[1].string
            kilometers_clean = kilometers.replace(".", "")

        elementDate = soup.find_all("hgroup")[0].find_all("h3")[0].string
        untilDate = (
            datetime.strptime(elementDate[-10:], "%d.%m.%Y")
            if elementDate
            else datetime.now()
        )

        return [untilDate, int(kilometers_clean)]
    except Exception as e:
        print(f"Error getting data for {city}: {e}")
        return None


def gather_data(cities):
    # Sammelt Daten für eine Liste von Städten und speichert sie in einem DataFrame."""
    results = []
    # timestamp = datetime.now().strftime("%Y")
    for [city, name] in cities:
        result = getKilometerFromCity(city)
        if result != None:
            [untilDate, kilometers] = result
            # if today is before the untilDate, ignore the data
            if datetime.now() < untilDate:
                print(f"Stadtradeln {city} has not ended yet, ignoring data")
                continue
            # get year from datetime
            year = untilDate.year
            if kilometers != None and kilometers != 0:
                results.append(
                    {"key": city, "year": year, "name": name, "value": kilometers}
                )
                print(f"{city}: {kilometers} km")
    return pd.DataFrame(results)


def save_to_csv(df):
    """Speichert den DataFrame als CSV-Datei."""
    existing_df = pd.read_csv("data/stadtradeln_data.csv")
    new_df = pd.concat([existing_df, df])
    new_df.reset_index(drop=True, inplace=True)
    new_df.sort_values(["key", "year"], inplace=True)
    new_df.drop_duplicates(subset=["key", "year"], keep="last", inplace=True)
    new_df.to_csv("data/stadtradeln_data.csv", index=False)
    if output_dir:
        new_df.to_csv(output_dir + "/stadtradeln_data.csv", index=False)


## make all cities lowercase
cities = [
    ["augsburg", "Augsburg"],
    ["kiel", "Kiel"],
    ["ulm", "Ulm"],
    ["bielefeld", "Bielefeld"],
    ["mannheim", "Mannheim"],
    ["luebeck", "Lübeck"],
    ["bonn", "Bonn"],
    ["potsdam", "Potsdam"],
    ["aachen", "Aachen"],
    ["osnabrueck", "Osnabrück"],
    ["wuppertal", "Wuppertal"],
    ["heidelberg", "Heidelberg"],
    ["braunschweig", "Braunschweig"],
    ["muenster", "Münster"],
    ["wiesbaden", "Wiesbaden"],
]

data = gather_data(cities)

save_to_csv(data)

driver.close()
driver.quit()
