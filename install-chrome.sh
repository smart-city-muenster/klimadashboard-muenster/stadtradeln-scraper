#!/bin/sh

apt update -y
apt install -y wget

wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
apt install -y ./google-chrome*.deb

rm google-chrome*.deb